(function(){
    'use strict';
    
//    ----- VARIABLES -----
    
    var addNew;
    var count;
    var storageLength = localStorage.length;
    var locationList = [];
    var locationGeometry;
    // local storage data
    var local_data = JSON.parse(localStorage.getItem('data'));
    // bool flag for knowing if there reviews on locations in the city that is searched.
    // keep false unless the city is found.
    var is_exist = false;
    
    // review bubble structure parametres
    var getParentElement;
    var locationElementUnit;
    var profileSummary;
    var profileFirstName;
    var profileLastName;
    var summaryLoc;
    var nameLoc;
    var descriptionLoc;
    var adress;
    var editBtn;
    var hlink;
    
//    ----- FUNCTIONS -----
    
    function init(){
        var arr = Object.keys(local_data.locations);
        console.log("key: " + arr);
        
        for( var i=0; i<arr.length; i++)
            {
                console.log("key: " + arr[i]);
                
            }
    }

    function addNewLocation() {
        window.location.assign("add-new.html");
    }
    
    // Pulling the JSON data of the speciefic location
    function pullLocationData(user_city_id){
        
        // clears the whole reviews container before starting to post.
        var clear_all = document.getElementById("reviews-container");
        // (future to be) array of saved placeID's pulled from the localStorage.
        var data_placeID;
        

        if (local_data === false)
            {
                local_data = {  locations:{}    };
            }
        else {
            if (user_city_id === false)
                {
                    console.log("error! no id. try to rewrite the search");
                    return
                }
            else {
                
// WORKS!!! looping through the cities and showing reviews
                /*
                for(var data_place_id in local_data.locations)
                    {
                        console.log("ID's: ",data_place_id,user_city_id);
                        
                        if (user_city_id === data_place_id)
                            {
                                clear_all.innerHTML = "";
                                is_exist = true;
                                console.log("is exist - is true");
                                var reviews = local_data.locations[data_place_id].reviews;
                                for (var review_id in reviews)
                                    {
                                        CreateLocationElement(reviews[review_id].f_name, reviews[review_id].l_name, 
                                                              reviews[review_id].title , reviews[review_id].description, review_id, data_place_id);
                                    }
                                break;
                            }
                        else
                            {
                                clear_all.innerHTML = "";
                                is_exist = false;
                                CreateLocationElement();
                            }
                    }
                    */
                
                // gathering the saved cities(placeID's) to one array.
                 data_placeID   = Object.keys(local_data.locations);
                 var is_valid   = function() {
                     for(var i=0; i<data_placeID.length; i++)
                         {
                             if (data_placeID[i] !== user_city_id){
                                 is_valid = false;
                             }
                             else
                                 {
                                     is_valid = true;
                                     return 
                                 }
                         }  
                    };is_valid();
                console.log('------------ ' + is_valid)
                 if (is_valid === true)
                     {
                        clear_all.innerHTML = "";
                        is_exist = true;
                        var reviews = local_data.locations[user_city_id].reviews;
                        for (var review_id in reviews)
                            {
                                CreateLocationElement(reviews[review_id].f_name, reviews[review_id].l_name, 
                                                      reviews[review_id].title , reviews[review_id].description, review_id, user_city_id);
                            }
                     }
                  else
                      {
                        clear_all.innerHTML = "";
                        is_exist = false;
                        CreateLocationElement();
                      }
                }
                
             }
    }
    
    function CreateLocationElement(first_name, last_name, location, description, review_id, curr_place_id){
        // TODO: make a slight delay between each review
/*        var animation_style;
        if (review_id == 0){
            animation_style = "";
        }
        else{
            
        }*/
        
    // ---- FUNCTION's VARIABLES ----
        
        getParentElement                = document.querySelector(".location-reviews");
        // div class='location-unit'
        locationElementUnit             = document.createElement("div");
        locationElementUnit.className   = 'location-unit ' + 'slide-div';
//        locationElementUnit.id          = 'el2'
        // same class as 'location-summary' for 'profile summary'
        profileSummary                  = document.createElement("div");
        profileSummary.className        = 'profile-summary';
        profileSummary.id               = 'profile-summary';
        // div class='first-name'
        profileFirstName                = document.createElement("div");
        profileFirstName.className      = 'location-name';
        profileFirstName.id             = 'first-name';
        // div class='last-name'
        profileLastName                 = document.createElement("div");
        profileLastName.className       = 'location-description';
        profileLastName.id              = 'last-name';
        // div class='location-summary'
        summaryLoc                      = document.createElement("div");
        summaryLoc.className            = 'location-summary';
        summaryLoc.id                   = 'location-summary';
        // div class='location-name'
        nameLoc                         = document.createElement("div");
        nameLoc.className               ='location-name';
        // div class='location-description'
        descriptionLoc                  = document.createElement("div");
        descriptionLoc.className        ='location-desription';
        // div class='location-address'
        adress                          = document.createElement("div");
        adress.className                ='location-adress';
        // input class='pretty-btn'
        editBtn                         = document.createElement("input");
        editBtn.className               = 'pretty-btn';
        editBtn.type                    = 'button';
        editBtn.value                   = 'Edit Review';
        editBtn.id                      = 'edit-btn'+review_id;
        editBtn.name                    = '?review_id='+review_id+'&place_id='+curr_place_id;
            
        hlink                           = document.createElement("a");
        hlink.setAttribute("id","adress-link");
        hlink.setAttribute("href","");
        
    // ---- PUSHING DATA TO THE CONTAINERS ----

        if (is_exist === false){
            var msgDiv  = document.createElement('div');
            var msg     = document.createElement('h1');
            
            msgDiv.className    = 'no-reviews';
            msg.className       = 'app-title';
            msg.innerHTML       = 'No reviews yet. Be the first to add :)';
            
            
            // if no reviews show div that says no reviews
            msgDiv.appendChild(msg);
            locationElementUnit.appendChild(msgDiv);
            getParentElement.appendChild(locationElementUnit);
        }
        else {
            // ---- MAKING THE STRUCTURE OF THE LOCATION's LIST ----
            // main element (locatioin-reviews -> location-unit)
            getParentElement.appendChild(locationElementUnit);

            // profileSummary -> first-name+last
            profileSummary.appendChild(profileFirstName);
            profileSummary.appendChild(profileLastName);

            //summaryLoc -> name+description
            summaryLoc.appendChild(nameLoc);
            summaryLoc.appendChild(descriptionLoc);

            // location-adress -> (a href + editBtn)
            adress.appendChild(hlink);
            adress.appendChild(editBtn);

            //location-unit -> profileSummary
            locationElementUnit.appendChild(profileSummary);

            //location-unit -> summaryLoc
            locationElementUnit.appendChild(summaryLoc);

            // location-unit -> adress
            locationElementUnit.appendChild(adress);

            document.querySelector('#edit-btn'+review_id).addEventListener('click', function () {   
                                                    goEdit(this.name);
                                            });

            profileFirstName.innerHTML  = first_name;
            profileLastName.innerHTML   = last_name;

            nameLoc.innerHTML           = location;
            descriptionLoc.innerHTML    = description;
    }
}
    
// adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  // Create the search box and link it to the UI element.
  var input = document.getElementById('search-bar');
  var searchBox = new google.maps.places.SearchBox(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();
    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
        // initialise the page population by reviews
        pullLocationData(place.place_id);
        
        // pass the 'place_id' and then extract all the geological data from it
        // with google getPlaces().
        // passGeometry(place.place_id);
        
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
        // 'return false' prevents from 'foreach' continuing to more than 1 location
        // so it would show only 1 location at a time.
        return false
    });
    map.fitBounds(bounds);
  });
}
    /*function passGeometry(placeID) {
        localStorage.setItem("place-id", JSON.stringify(placeID));
    }*/
    
//    ** initilize GOOGLE MAPS API **
    /*  
    function initialiseMap() {
        
    // ---- FUNCTION's VARIABLES ----
        // storing the position data
//        var currPos = new google.maps.LatLng(35, 35);
        var currPos;
        // when navigator succeded
        var geoSuccess = function(location){
            currPos = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
            console.log("LAT: " + location.coords.latitude + " " + "LON: " + location.coords.longitude);
            
            
        };
        // when navigator failed
        var geoError = function(error) {
            console.log('Error occurred. Error code: ' + error.code);
            // error.code can be:
            //   0: unknown error
            //   1: permission denied
            //   2: position unavailable (error response from location provider)
            //   3: timed out
        };
        // navigator's options
        var geoOptions = {
            maximumAge: 5 * 60 * 1000,
        };
        // map properties
        var mapProp = {
            center: currPos,
            zoom:5,
            mapTypeId:google.maps.MapTypeId.ROADMAP
            };
        console.log("ok1");
        navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
        console.log("ok1");
        var map = new google.maps.Map(document.querySelector(".gmap"),mapProp);
    }*/
    
    // REDIRECT'S TO EDIT PAGE AND PASS A POSITION OF THE DATA (that needs to be changed) IN THE LOCAL STORAGE
    
    function goEdit(review_data) {
        window.location.assign('edit.html'+review_data);
    }

//    ----- MAIN CODE -----
    
    google.maps.event.addDomListener(window, 'load', initAutocomplete);
    document.querySelector("#btn-add-new").addEventListener('click', addNewLocation);
    init();

})();