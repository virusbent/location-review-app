(function(){
    'use strict';
    
    var local_data = JSON.parse(localStorage.getItem("data"));
    if(!local_data)     
        local_data = {  locations:{}    }
        
    var city;
/*    var newLocationData = {
        profile   : { firstN: "", lastN: "" },
        location  : { locName: "", description: "" },
        placeID   : ""
    };
    
    var new_LocationData = {
        placeID    : {
            formated_adress    : "",
            lat                : "",
            lng                : "",
            reviews     : [
                {
                    id          : "",
                    title       : "",
                    description : "",
                    img_url     : "",
                    f_name      : "",
                    l_name      : ""
                },
                
                {
                    id          : "",
                    title       : "",
                    description : "",
                    img_url     : "",
                    f_name      : "",
                    l_name      : ""
                }
            ]
        }
    };*/
    
    // place id unique key
    var googlePlaceID = pullPlaceID();
    
    // pulling place id from the local storage
    function pullPlaceID(){
        // place id of TEL AVIV for default
        var default_id = 'ChIJH3w7GaZMHRURkD-WwKJy-8E';
        var id_is_valid = JSON.parse(localStorage.getItem("place-id"));
        if (id_is_valid)
            {
                return id_is_valid
            }
        else 
            {
                console.log("place ID is invalid! showing Tel-Aviv by default");
                return default_id
            }
    }
  
    // Initialise Google Maps services
    function initialise() {
        var map = new google.maps.Map(document.getElementById("map-canvas"), {
          center: new google.maps.LatLng(32.0808, 34.7805),
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        var options = { types: ['(cities)'] };
        var input = document.getElementById('city');
        var searchBox = new google.maps.places.SearchBox(input, options);
        
        // pass the location to the map viewport
        map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
        });
          var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
        searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();
        
        city = {
            placeID          : places[0].place_id,
            formatted_address: places[0].formatted_address,
            lat              : places[0].geometry.location.lat(),
            lng              : places[0].geometry.location.lng()
        }
            
        if (places.length == 0) {
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
        // 'return false' prevents from 'foreach' continuing to more than 1 location
        // so it would show only 1 location at a time.
        // change if needed.
        return false
    });
    map.fitBounds(bounds);
  });
    var request = {
        placeId: googlePlaceID
    };
        
    /*var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);

        service.getDetails(request, function(place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
              var marker = new google.maps.Marker({
                map: map,
                position: place.geometry.location
              });
              google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(place.name);
                infowindow.open(map, this);
              });
              map.fitBounds(place.geometry.viewport);
            }
            // pushing the location name recieved with placeID to locationName input box
            document.getElementById("locationName").value = place.name;
        });*/
  }

  
/* ---- /END ---- */
    
    
    function init(){ document.getElementById("PassNewLocation").addEventListener('click', getData); }
    
    function getData() {
        
        // Array of all the inputs on the page BEFORE validation procces.
        var userFormInput   = document.querySelectorAll('.clean-slide');
        // Array of all the inputs on the page AFTER validation procces. 
        var userFormReady;

        var is_valid = false;
        
        for (var i = 0; i < (userFormInput.length - 1); i++ )
            {
                is_valid = validateForm(userFormInput[i].value, userFormInput[i].id);
                
                if(is_valid){
                    userFormReady[i] = userFormInput[i];
                }
                else {
                    userFormInput.className = ' red-border';
                    console.log('is_valid = false!');
                    
                }
            }
        
        if (!local_data.locations[city.placeID]) {
            local_data.locations[city.placeID] = city;
            local_data.locations[city.placeID].reviews = [];
        }
        

        var review = {
            id          :   local_data.locations[city.placeID].reviews.length,
            // The same order as the inputs on the page.
            f_name      :   userFormInput[0].value,
            l_name      :   userFormInput[1].value,
            title       :   userFormInput[2].value,
            description :   userFormInput[3].value,
            img_url     :   userFormInput[5].value
        }
        
        local_data.locations[city.placeID].reviews.push(review);
/*
        
        newLocationData.profile.firstN  = profileForm.fname.value;
        newLocationData.profile.lastN   = profileForm.lname.value;
        
        newLocationData.location.locName        = locationForm.locName.value;
        newLocationData.location.description    = locationForm.description.value;
*/
        
        passToLocalStorage();
    }
    
    function initSearchPlaces() {
        var input = document.getElementById('city');
        var searchBox = new google.maps.places.SearchBox(input);
        
        // pass th elocation to the map viewport
        map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
  });
    }
    
    function passToLocalStorage(){
        var objToJSON = JSON.stringify(local_data);
        localStorage.setItem("data", objToJSON);
        // redirect to the main page by the end of session
        window.location.href = "index.html";
    }
    
    init();
    google.maps.event.addDomListener(window, "load", initialise);
    
    // submit btn function

    

})();



