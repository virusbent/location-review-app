(function(){
    'use strict';
    
//    ----- VARIABLES -----
    // getting parametres from url query.
    var curr_place_id   = getParameterByName('place_id');
    var review_id       = getParameterByName('review_id');
    // Array of all the inputs on the page.(referenced by class clean-slide)
    var inputs          = document.querySelectorAll('.clean-slide');
    
//    ----- FUNCTIONS -----
    
function initilise() {
    pullDataFromLocalStorage(curr_place_id, review_id);
    // initializing google maps
    google.maps.event.addDomListener(window, "load", init_gmap);
    
}
    
// Pulling the parameteres from the url query (place_id and review_id)
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function pullDataFromLocalStorage(curr_city, review) {
    // Pulling review data from localStorage with current city place_id.
    var pulledInputData = JSON.parse(localStorage.getItem('data')).locations[curr_city];
    // Pushing the data to the inputs.
    inputs[0].value = pulledInputData.reviews[review].f_name;
    inputs[1].value = pulledInputData.reviews[review].l_name;
    inputs[2].value = pulledInputData.reviews[review].title;
    inputs[3].value = pulledInputData.reviews[review].description;
    inputs[4].value = pulledInputData.formatted_address;
    inputs[5].value = pulledInputData.reviews[review].img_url;
}
    
function saveData() {
    var editedData;
    var local_data = JSON.parse(localStorage.getItem('data'));
    
    local_data.locations[curr_place_id].reviews[review_id].f_name = inputs[0].value;
    local_data.locations[curr_place_id].reviews[review_id].l_name = inputs[1].value;
    local_data.locations[curr_place_id].reviews[review_id].title = inputs[2].value;
    local_data.locations[curr_place_id].reviews[review_id].description = inputs[3].value;
    local_data.locations[curr_place_id].formatted_address = inputs[4].value;
    local_data.locations[curr_place_id].reviews[review_id].img_url = inputs[5].value;
    
    console.log(JSON.stringify(local_data));
    editedData = JSON.stringify(local_data);
    localStorage.setItem('data', editedData);
}
    
function init_gmap() {
        var map = new google.maps.Map(document.getElementById("map-canvas"), {
          center: new google.maps.LatLng(32.0808, 34.7805),
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        var options = { types: ['(cities)'] };
        var input = document.getElementById('city');
        var searchBox = new google.maps.places.SearchBox(input, options);
        
        // pass the location to the map viewport
        map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
        });
          var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
        searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();
            
        if (places.length == 0) {
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
        // 'return false' prevents from 'foreach' continuing to more than 1 location
        // so it would show only 1 location at a time.
        // change if needed.
        return false
    });
    map.fitBounds(bounds);
  });
    var request = {
        placeId: curr_place_id
    };
  }
    
//    ----- MAIN CODE -----

    initilise();
    document.getElementById("PassNewLocation").addEventListener('click', saveData);
    
})();